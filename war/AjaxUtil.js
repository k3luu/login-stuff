function createXHR() {
	var xmlhttp;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xmlhttp;
}

function deleteLoginEntry() {
	var delXhrObj = createXHR();
	delXhrObj.onreadystatechange = function() {
		if (delXhrObj.readyState == 4 && delXhrObj.status == 200) {
			displayListView();
		}
	}
	delXhrObj.open("POST", "/login", true);
	delXhrObj.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	delXhrObj.send("category=" + this.id + "&action=DELETE");
}

function displayCreateLoginTab(create) {
	cP();
	var createLoginContentDiv = document
			.getElementById('createContentLogins');
	var createItemContentDiv = document.getElementById('createContentItems');
	createLoginContentDiv.style.display = 'block';
	createItemContentDiv.style.display = "none";
	var listViewDiv = document.getElementById('listView');
	var createViewDiv = document.getElementById('createView');

	listViewDiv.style.display = 'none';
	createViewDiv.style.display = 'block';
	if (typeof create === 'object') {
		populateCreateLogValues(this);
	} else {
		populateCreateLogValues();
	}
}

function populateCreateLogValues(oThis) {
	document.getElementById('category').value = oThis ? oThis.innerHTML : '';
	//document.getElementById('description').value = oThis ? oThis.parentNode.nextSibling.innerHTML: '';
	
	// WEIJIE     Place-it 
	//document.getElementById('placeItTitle').value = oThis ? oThis.innerHTML : '';
	document.getElementById('pWord').value = oThis ? oThis.parentNode.nextSibling.innerHTML : '';
}


function displayListView() {
	var welcomeViewDiv = document.getElementById('welcomeView');
	var listViewDiv = document.getElementById('listView');
	var createViewDiv = document.getElementById('createView');

	welcomeViewDiv.style.display = 'none';
	listViewDiv.style.display = 'block';
	createViewDiv.style.display = 'none';
	var xhrObj = createXHR();
	xhrObj.open("GET", "/login", true);
	xhrObj.send();
	xhrObj.onreadystatechange = function() {
		if (xhrObj.readyState == 4 && xhrObj.status == 200) {
			var data = '';
			if (xhrObj.responseText.trim() && xhrObj.responseText != '\r\n') {
				data = eval('(' + xhrObj.responseText + ')');
			}
			var currTab = document.getElementById('listViewLoginTBody');
			currTab.innerHTML = '';
			currTab.innerHTML = '';
			for ( var prop in data) {
				var currTrData = data[prop];

				var delLink = document.createElement('div');
				delLink.id = currTrData.name;
				delLink.style.outline = 'none';
				delLink.style.color = 'blue';
				delLink.style.cursor = 'pointer';
				delLink.style.display = 'inline';
				delLink.innerHTML = 'Delete';
				delLink.onclick = deleteLoginEntry;

				var oTr = document.createElement('tr');
				var nameTd = document.createElement('td');
				nameTd.innerHTML = currTrData.name;
				oTr.appendChild(nameTd);
				//var descTd = document.createElement('td');
				//descTd.innerHTML = currTrData.Description;
				//oTr.appendChild(descTd);
				
				//var placeItTitleTd = document.createElement('td');
				//iplaceItTitleTd.innerHTML = currTrData.placeItTitle;
				//oTr.appendChild(placeItTitleTd);
				
				var pWordTd = document.createElement('td');
				pWordTd.innerHTML = currTrData.pWord;
				oTr.appendChild(pWordTd);
				
				// Ended
				
				var deleteTd = document.createElement('td');
				deleteTd.appendChild(delLink);
				oTr.appendChild(deleteTd);

				currTab.appendChild(oTr);
			}
		}
	}
}

function displayCreateView() {
	var welcomeViewDiv = document.getElementById('welcomeView');
	var listViewDiv = document.getElementById('listView');
	var createViewDiv = document.getElementById('createView');

	welcomeViewDiv.style.display = 'none'
	listViewDiv.style.display = 'none';
	createViewDiv.style.display = 'block';
	displayCreateLoginTab(true);
}

function saveLogin() {
	var xmlhttp = createXHR();
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			displayListView();
		}
	}
	xmlhttp.open("POST", "/login", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("category=" + document.getElementById('category').value
			+ "&pWord=" + document.getElementById('pWord'));
	
	document.getElementById('pWord').value= "";
}


function displayListViewByType(typeStr) {
	if (typeStr == 'logins') {
		document.getElementById('logins').style.display = "block";
	} 
}

function cP() {
	document.getElementById('createLoginTab').className = "normalTab activeTab";
}
