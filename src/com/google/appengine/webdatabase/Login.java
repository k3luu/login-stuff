package com.google.appengine.webdatabase;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;

import java.util.List;

/**
 * This class handles all the CRUD operations related to
 * Login entity.
 *
 */
public class Login {

  /**
   * Update the login
   * @param user: username of the login
   * @param password : password of the login
   * @return  updated login
   */
  public static void createOrUpdateLogin(String user, String password) {
    Entity login = getLogin(user);
  	if (login == null) {
  	  login = new Entity("Login", user);
  	  System.out.println("1. The key is: "+login.getKey());

  	  login.setProperty("pWord", password);
  	} else {
	  login.setProperty("pWord", password);
  	}
  	Util.persistEntity(login);
  }

  /**
   * Return all the logins
   * @param kind : of kind login
   * @return  login
   */
  public static Iterable<Entity> getAllLogins(String kind) {
    return Util.listEntities(kind, null, null);
  }

  /**
   * Get login entity
   * @param name : user name of the login
   * @return: login entity
   */
  public static Entity getLogin(String name) {
  	Key key = KeyFactory.createKey("Login",name);
  	return Util.findEntity(key);
  }

  /**
   * Get all items for a login
   * @param name: user name of the login
   * @return list of logins
   */
  
  public static List<Entity> getItems(String name) {
  	Query query = new Query();
  	Key parentKey = KeyFactory.createKey("Login", name);
  	query.setAncestor(parentKey);
  	query.addFilter(Entity.KEY_RESERVED_PROPERTY, Query.FilterOperator.GREATER_THAN, parentKey);
  		List<Entity> results = Util.getDatastoreServiceInstance()
  				.prepare(query).asList(FetchOptions.Builder.withDefaults());
  		return results;
	}
  
  /**
   * Delete login entity
   * @param loginKey: login to be deleted
   * @return status string
   */
  public static String deleteLogin(String loginKey)
  {
	  Key key = KeyFactory.createKey("Login",loginKey);	   
	  
	  List<Entity> items = getItems(loginKey);	  
	  if (!items.isEmpty()){
	      return "Cannot delete, as there are items associated with this login.";	      
	    }	    
	  Util.deleteEntity(key);
	  return "Login deleted successfully";
	  
  }
}
